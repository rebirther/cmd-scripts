chcp 1251
@echo off

cls
echo ===================================================
echo ==== Create access point Wi-Fi on Windows 8,10 ====
echo ===================================================
echo.

goto check_Permissions

:check_Permissions

    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo.
    ) else (
	echo The script must be run as administrator...
	echo.
	echo.
        echo Error: Requires elevated privileges.
	pause >nul
	exit
    )

:stype
echo Select:
echo 1. The default name and password.
echo 2. Its own parameters.
set /p type=">: "
echo.

if not defined type goto stype
if "%type%"=="1" (goto sdefault)
if "%type%"=="2" (goto sconf)

:sdefault
set name=IBM
set pass=123456aaa
echo SSID: %name%
echo Password: %pass%
netsh wlan set hostednetwork mode=allow ssid=%name% key=%pass%

:start1
echo Select:
echo 1. Run the created network.
echo 2. Change network settings.
set /p step2=">: "

if not defined step2 goto start1
if "%step2%"=="1" (goto start)
if "%step2%"=="2" (goto sconf)

:sconf
set /p name="Enter the network name. >:"
echo SSID: %name%
echo.
echo The password can only contain letters and numbers. 
echo Password length of 8 characters. 
echo.
set /p pass="Enter password >:"
echo Your password: %pass%
echo.
echo Click to create a network
pause
netsh wlan set hostednetwork mode=allow ssid=%name% key=%pass%

:start2
echo Select:
echo 1. Run the created network.
echo 2. Change the entered data.
set /p step2=">: "

if not defined step2 goto start2
if "%step2%"=="1" (goto start)
if "%step2%"=="2" (goto sconf)

:start
netsh wlan start hostednetwork
pause